import java.util.Scanner;

public class Addition{
	public static void main(String args[]){
		int num1, num2;
		int result;
		
		Scanner sc = new Scanner(System.in);
		
		System.out.print("Enter an integer value please: ");
		num1 = sc.nextInt();
		System.out.print("Enter another integer value please: ");
		num2 = sc.nextInt();
		
		result = num1 + num2;
		
		System.out.printf( "The sum of %d and %d is %d", num1, num2, result );
	}
}