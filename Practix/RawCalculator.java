import java.util.Scanner;

public class RawCalculator{
	public static void main( String args[] ) {
		Scanner input = new Scanner(System.in);
		
		int num1, num2;
		double difference, sum, quotient, product;
		int modulos;
		
		System.out.print( "Please enter a number: " ) ;
		num1 = input.nextInt();
		System.out.println();
		System.out.print( "Please enter another number: " );
		num2 = input.nextInt();
		System.out.println();
		
		sum = num1 + num2;
		difference = num2 - num1;
		quotient = num1 / num2;
		product = num1 * num2;
		modulos = num1 % num2;
		
		System.out.printf( "The sum of the two numbers are %.2f\nThe difference between the two numbers is %.2f\nThe product is %.2f\nThe remainder is %d\n and the quotient is %.2f", sum, difference, product, modulos, quotient );
	}
}