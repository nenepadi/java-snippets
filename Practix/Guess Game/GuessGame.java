public class GuessGame{
  //setting the instance variables...
  Player play1, play2, play3;
  
  public void startGame(){
    play1 = new Player();
    play2 = new Player();
    play3 = new Player();
    
    int guessp1 = 0, guessp2 = 0, guessp3 = 0;
    
    boolean p1Right = false;
    boolean p2Right = false;
    boolean p3Right = false;
    
    int targetNum = (int) (Math.random() * 10);
    System.out.println("I am thinking of a number between 0 and 9....");
    
    while (true){
      System.out.println("The number to guess is " + targetNum);
      
      play1.guess();
      play2.guess();
      play3.guess();
      
      guessp1 = play1.number;
      System.out.println("Player 1 guessed " + guessp1);
      guessp2 = play2.number;
      System.out.println("Player 2 guessed " + guessp2);
      guessp3 = play3.number;
      System.out.println("Player 3 guessed " + guessp3);
      
      if (guessp1 == targetNum){
        p1Right = true;
      }
      if (guessp2 == targetNum){
        p2Right = true;
      }
      if (guessp3 == targetNum){
        p3Right = true;
      }
      
      if (p1Right || p2Right || p3Right){
        System.out.println("Hurray, we had a winner....The correct guess is " + targetNum + ". Find out who it was.");
        System.out.println("Player 1 guessed " + guessp1);
        System.out.println("Player 2 guessed " + guessp2);
        System.out.println("Player 3 guessed " + guessp3);
        System.out.println("Game over");
      } else {
        System.out.println("All players are loosers. Better lack next time.");
      }
    } //end of while loop
  } // end of method startGame()
} //end of class