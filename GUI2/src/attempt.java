import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Toolkit;

import javax.swing.*;
public class attempt extends JFrame{

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		attempt myFrame = new attempt();
		
		JMenuBar mb = new JMenuBar();
		JMenu fm = new JMenu("File");
		JMenuItem fm1 = new JMenuItem("New project");
		fm.add(fm1);
		JMenuItem fm2 = new JMenuItem("New File");
		fm.add(fm2);
		JMenuItem fm3 = new JMenuItem("Copy");
		fm.add(fm3);
		JMenuItem fm4 = new JMenuItem("Close");
		fm.add(fm4);
		mb.add(fm);
		myFrame.setJMenuBar(mb);
		
		//adding a text area...
		JTextArea myText = new JTextArea(6, 10);
		//myText.setText("The man is gone to west.");
		//myText.setEditable(true);
		JScrollPane sp = new JScrollPane(myText);	//adding a scroll pane to your text area
		myFrame.add(sp);
		
		//adding checkboxes...
		JCheckBox single = new JCheckBox("Single");
		myFrame.add(single);
		JCheckBox married = new JCheckBox("Married");
		myFrame.add(married);
		JCheckBox divorced = new JCheckBox("Divorced");
		myFrame.add(divorced);
		//the buttonGroup class will group all the checkboxes and let them behave as option buttons...
		ButtonGroup bg = new ButtonGroup();
		bg.add(single);
		bg.add(married);
		bg.add(divorced);
		
		//adding a password field...
		JLabel password = new JLabel("Password:");
		myFrame.add(password);
		JPasswordField ps = new JPasswordField(8);
		ps.setEchoChar('*');
		myFrame.add(ps);
		
		
		//customising our own fonts...
		JLabel font = new JLabel("My best sculpture");
		font.setFont(new Font("Monospace", Font.BOLD, 30));
		font.setForeground(Color.blue);
		myFrame.add(font);
		
		//changing the icon...
		myFrame.setIconImage(Toolkit.getDefaultToolkit().getImage("bb.jpg"));
		myFrame.setLayout(new FlowLayout());
		
		//adding an image...
		Icon myIcon = new ImageIcon("bb.jpg");
		JLabel myLabel = new JLabel(myIcon);
		myFrame.add(myLabel);
		
		String data[]={"Obed", "Pius", "Evans", "Nicho", "Miguel", "Peters"};
		JList myList = new JList(data);
		JScrollPane sc = new JScrollPane();
		myFrame.add(sc);
		
		//default frame...
		myFrame.setTitle("My App");
		myFrame.setSize(600, 400);
		myFrame.setDefaultCloseOperation(EXIT_ON_CLOSE);
		myFrame.setVisible(true);
		myFrame.setLocationRelativeTo(null);
	}

}
