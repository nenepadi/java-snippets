package drawsmiley;

import javax.swing.*;
public class DrawSmiley {
	
	public static void main(String[] args) {
		Smiley mySmiley = new Smiley();
		JFrame application = new JFrame();
		
		application.setTitle("Smiley");
		application.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		application.add(mySmiley);
		application.setSize(230,250);
		application.setVisible(true);

	}

}
