import java.awt.Graphics; //Importing java's Graphics class.
import javax.swing.JPanel; //JPanel, another container from the GUI elements of java.

//Shapes class we want to create must inherit from the JPanel class from java...
public class Shapes extends JPanel{
	private int choice; //an instance variable to handle user's choice...
	
	//a method which is more of a constructor...
	public Shapes(int userChoice){
		choice = userChoice;
	}
	
	//a method which controls how objects from the Graphics class are drawn...
	public void paintComponent(Graphics g){
		super.paintComponent(g);
		
		for(int i = 0; i < 10; i++){
			switch(choice){
				case 1:
					//draw rectangle...
					g.drawRect(10+i*10, 10+i*10, 50+1*10, 50+1*10);
					break;
				case 2:
					//draw an oval...
					g.drawOval(10+i*5, 10+i*5, 30*i*5, 30+i*5);
					break;
			}
		}
	}
}
