/*
 * Purpose: Testing the some Graphics elements in Java
 * Maintainer: kizit2012@gmail.com
 * */

//import the classes and packages needed..
import javax.swing.JFrame;
import javax.swing.JOptionPane;

public class ObjectDraw {
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		String input = JOptionPane.showInputDialog("Enter 1 to draw rectangles.\nEnter 2 to draw circles.");
		int choice = Integer.parseInt(input);
		
		Shapes panel = new Shapes(choice); //creating an instance of the Shapes class we created...
		JFrame application = new JFrame(); //creating an instance of JFrame...
		
		application.setTitle("Drawing Shapes"); //set the title of the frame...
		application.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); //close frame when the close button is clicked...
		application.add(panel); //add the new Shapes instance as a component to our frame...
		application.setSize(300,300); //set size of frame...
		application.setVisible(true); //make frame visible...
		
	}

}
