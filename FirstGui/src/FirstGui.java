/*
 * Group: Was done in the lab with the lecturer.
 * Purpose: Program to add two numbers.
 * Maintainer: kizit2012@gmail.com
 * 
 * */

import javax.swing.*;
import java.awt.*;

public class FirstGui extends JFrame {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		FirstGui gui = new FirstGui();
		
		JLabel label = new JLabel("First name: ");
		gui.add(label);
		
		JTextField tf = new JTextField (10);
		gui.add(tf);
		
		JButton bt = new JButton("Button");
		gui.add(bt);
		
		JComboBox combo = new JComboBox();
		combo.addItem("Yes");
		combo.addItem("No");
		gui.add(combo);
		
		
		gui.setLayout(new FlowLayout());
		
		gui.setTitle("My first GUI App");
		gui.setSize(600, 400);
		gui.setDefaultCloseOperation(EXIT_ON_CLOSE);
		gui.setVisible(true);
		gui.setLocationRelativeTo(null);

	}

}
