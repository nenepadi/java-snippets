package laying;

import java.awt.*;

import javax.swing.*;

public class LayManager extends JFrame {
	private JPanel topPanel;
	private JPanel bottom;
	private JTextField txt;
	private JLabel passwd;
	private JLabel label;
	private JPasswordField pass;
	private JButton ok;
	private JButton cancel;
	
	public LayManager(){
		topPanel = new JPanel();
		topPanel.setLayout(new GridLayout(4,1));
		label = new JLabel("Username");
		topPanel.add(label);
		txt = new JTextField(10);
		topPanel.add(txt);
		passwd = new JLabel("Password");
		topPanel.add(passwd);
		pass = new JPasswordField(10);
		topPanel.add(pass);
		
		add(topPanel, BorderLayout.CENTER);
		
		bottom = new JPanel();
		bottom.setLayout(new FlowLayout(FlowLayout.RIGHT));
		ok = new JButton("OK");
		bottom.add(ok);
		cancel = new JButton("CANCEL");
		bottom.add(cancel);
		
		add(bottom, BorderLayout.SOUTH);
		
		
		
	}
}
