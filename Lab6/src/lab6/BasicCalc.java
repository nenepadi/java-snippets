package lab6;

import java.awt.*;
import javax.swing.*;

public class BasicCalc extends JFrame {
	private JPanel display;
	private JPanel operand;
	private JPanel operator;
	private JTextField txt;
	//operands...
	private JButton bt0;
	private JButton bt1;
	private JButton bt2;
	private JButton bt3;
	private JButton bt4;
	private JButton bt5;
	private JButton bt6;
	private JButton bt7;
	private JButton bt8;
	private JButton bt9;
	private JButton btCls;
	//operators...
	private JButton multi;
	private JButton divi;
	private JButton addi;
	private JButton subt;
	private JButton equi;
	
	
	public BasicCalc(){
		//display panel
		display = new JPanel();
		txt = new JTextField(50);
		txt.setBackground(Color.orange);
		display.add(txt);
		add(display, BorderLayout.NORTH);
		
		//operands panel
		operand = new JPanel();
		operand.setLayout(new GridLayout(4,3));
		bt1 = new JButton("1");
		operand.add(bt1);
		bt2 = new JButton("2");
		operand.add(bt2);
		bt3 = new JButton("3");
		operand.add(bt3);
		bt4 = new JButton("4");
		operand.add(bt4);
		bt5 = new JButton("5");
		operand.add(bt5);
		bt6 = new JButton("6");
		operand.add(bt6);
		bt7 = new JButton("7");
		operand.add(bt7);
		bt8 = new JButton("8");
		operand.add(bt8);
		bt9 = new JButton("9");
		operand.add(bt9);
		bt0 = new JButton("0");
		operand.add(bt0);
		btCls = new JButton("CLS");
		operand.add(btCls);
		add(operand, BorderLayout.CENTER);
		
		//operators panel...
		operator = new JPanel();
		operator.setLayout(new GridLayout(5,1));
		multi = new JButton("*");
		multi.setBackground(Color.yellow);
		operator.add(multi);
		divi = new JButton("/");
		divi.setBackground(Color.yellow);
		operator.add(divi);
		addi = new JButton("+");
		addi.setBackground(Color.yellow);
		operator.add(addi);
		subt = new JButton("-");
		subt.setBackground(Color.yellow);
		operator.add(subt);
		equi = new JButton("=");
		equi.setBackground(Color.red);
		operator.add(equi);
		add(operator, BorderLayout.EAST);
		
		
		
		
		
		
	}

}
