package lab6;

import javax.swing.JFrame;


public class BasicCalcTest {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		BasicCalc application = new BasicCalc();
		
		application.setTitle("Calculator");
		application.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		application.setSize(600, 600);
		application.setVisible(true);
		application.setLocationRelativeTo(null);

	}

}
