/*
 * Group: Was done in the lab with the lecturer.
 * Purpose: Program to add two numbers.
 * Maintainer: kizit2012@gmail.com
 * 
 * */

//importing packages or classes that we need...
import javax.swing.JOptionPane; //JOptionPane displays a dialog to the user...

public class Addition {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		/*
		 * Just like we do in CLI with the Scanner class,
		 * We ask the user to enter an input...
		 * JOption accepts only strings so to get the right format
		 * We parse the input by the user to integer or number..
		 * */
		String num1 = JOptionPane.showInputDialog("Please enter an integer");  //accepting input
		int firstNum = Integer.parseInt(num1);  //parsing input to number
		
		//Second input...
		String num2 = JOptionPane.showInputDialog("Please enter an integer");
		int secondNum = Integer.parseInt(num2);
		
		//adding numbers..
		int sum = firstNum + secondNum;
		
		//displaying an result of addition...
		JOptionPane.showMessageDialog(null, "The sum is " + sum, "Sum of two integers", JOptionPane.INFORMATION_MESSAGE);
		
		

	}

}
