package employee;

import java.awt.*;
import javax.swing.*;
public class EmployeeReg extends JFrame {
	private JLabel empID;
	private JTextField txtID;
	private JLabel title;
	private JComboBox cmbTitle;
	private JLabel address;
	private JTextArea txtAddress;
	private JLabel fname;
	private JTextField txtFname;
	private JLabel lname;
	private JTextField txtLname;
	private JPanel status;
	private JRadioButton single;
	private JRadioButton married;
	private JRadioButton divorced;
	private JLabel city;
	private JTextField txtCity;
	private JLabel country;
	private JTextField txtCountry;
	private JLabel email;
	private JTextField txtEmail;
	private JLabel tel;
	private JTextField txtTel;
	private JButton ok;
	private JButton cancel;
	
	public EmployeeReg(){
		
		setLayout(null);
		
		empID = new JLabel("Employee ID");
		empID.setBounds(20,20,150,15);
		add(empID);
		txtID = new JTextField();
		txtID.setBounds(120,20,150,15);
		add(txtID);
		
		title = new JLabel("Title: ");
		title.setBounds(20,40,150,15);
		add(title);
		cmbTitle = new JComboBox();
		cmbTitle.addItem("Mr.");
		cmbTitle.addItem("Mrs.");
		cmbTitle.addItem("Miss");
		cmbTitle.addItem("Rev.");
		cmbTitle.setBounds(120, 40, 150, 15);
		add(cmbTitle);
		
		fname = new JLabel("First name: ");
		fname.setBounds(20, 60, 150, 15);
		add(fname);
		txtFname = new JTextField(15);
		txtFname.setBounds(120, 60, 150, 15);
		add(txtFname);
		
		lname = new JLabel("Last name: ");
		lname.setBounds(400, 60, 150, 15);
		add(lname);
		txtLname = new JTextField();
		txtLname.setBounds(500, 60, 150, 15);
		add(txtLname);
		
		address = new JLabel("Address: ");
		address.setBounds(20, 80, 150, 15);
		add(address);
		txtAddress = new JTextArea();
		txtAddress.setBounds(120,80,150,60);
		add(txtAddress);
		
		status = new JPanel();
		single = new JRadioButton("Single");
		status.add(single);
		married = new JRadioButton("Married");
		status.add(married);
		divorced = new JRadioButton("Divorced");
		status.add(divorced);
		status.setBounds(400, 80, 300, 60);
		status.setBorder(BorderFactory.createTitledBorder("Marital status"));
		add(status);
		
		city = new JLabel("City");
		city.setBounds(20, 150, 150, 15);
		add(city);
		txtCity = new JTextField();
		txtCity.setBounds(120, 150, 150, 15);
		add(txtCity);
		
		country = new JLabel("Country");
		country.setBounds(20, 170, 150, 15);
		add(country);
		txtCountry = new JTextField();
		txtCountry.setBounds(120, 170, 150, 15);
		add(txtCountry);
		
		email = new JLabel("Email");
		email.setBounds(20, 190, 150, 15);
		add(email);
		txtEmail = new JTextField();
		txtEmail.setBounds(120, 190, 150, 15);
		add(txtEmail);
		
		tel = new JLabel("Telephone");
		tel.setBounds(20, 210, 150, 15);
		add(tel);
		txtTel = new JTextField();
		txtTel.setBounds(120, 210, 150, 15);
		add(txtTel);
		
		ok = new JButton("OK");
		ok.setBounds(400, 250, 80, 20);
		add(ok);
		cancel = new JButton("CANCEL");
		cancel.setBounds(540, 250, 80, 20);
		add(cancel);
	}

}
